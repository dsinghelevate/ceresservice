package com.caeldemo.ceresservice;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

class BaseControllerTest {

    @Test
    @Tag("minimal")
    @Tag("production")
    @DisplayName("Test Case 1")
    public void methodOne_testcase1() {
        assertTrue(true);
    }

    @Test
    @Tag("minimal")
    @DisplayName("Test Case 2")
    public void methodOne_testcase2() {
        assertTrue(true);
    }

    @Test
    @Tag("minimal")
    @DisplayName("Test Case 3")
    public void methodOne_testcase3() {
        assertTrue(true);
    }

    @Test
    @DisplayName("Test Case 4")
    public void methodOne_testcase4() {
        assertTrue(true);
    }

    @Test
    @DisplayName("Test Case 5")
    public void methodOne_testcase5() {
        assertTrue(true);
    }

    @Test
    @DisplayName("Test Case 5")
    public void methodOne_testcase6() {
        assertTrue(true);
    }
}