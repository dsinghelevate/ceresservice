package com.caeldemo.ceresservice;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SecondControllerIntTest {

    @Test
    @DisplayName("Test Case 1")
    @Tag("minimal")
    @Tag("production")
    public void methodOne_testcase1() {
        assertTrue(true);
    }

    @Test
    @DisplayName("Test Case 2")
    @Tag("minimal")
    public void methodOne_testcase2() {
        assertTrue(true);
    }

    @Test
    @DisplayName("Test Case 3")
    public void methodOne_testcase3() {
        assertTrue(true);
    }

    @Test
    @DisplayName("Test Case 4")
    public void methodOne_testcase4() {
        assertTrue(true);
    }
}