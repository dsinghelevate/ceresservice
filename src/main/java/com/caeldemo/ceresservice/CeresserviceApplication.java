package com.caeldemo.ceresservice;

import com.caeldemo.ceresservice.config.Ceres;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(value = Ceres.class)
public class CeresserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CeresserviceApplication.class, args);
    }

}
