package com.caeldemo.ceresservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "my")
public @Data class Ceres {

    private String serverip;
    private String serverport;
}
