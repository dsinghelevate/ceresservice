package com.caeldemo.ceresservice;

import com.caeldemo.ceresservice.config.Ceres;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class BaseController {

    @Autowired
    private Ceres ceres;

    @Value("${cael.message}")
    private String msg;

    @GetMapping("/welcome")
    public Ceres methodOne()
    {
        return ceres;
    }

    @GetMapping("/showgreetings/{name}")
    public String methodTwo(@PathVariable("name") String mname)
    {
        System.out.println(msg);
        return "Service Ceres Entered name is "+mname.toUpperCase();
    }
}
